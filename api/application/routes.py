from flask import Blueprint, jsonify, request

from application.app import db
from application.database import User

view = Blueprint("view", __name__)


@view.route("/")
def home():
    return "iBroHW!"

@view.route("/registration", methods=["POST"])
def registration():
    pass

@view.route("/login", methods=["POST"])
def login():
    pass

@view.route("/items/new", methods=["POST"])
def items_new():
    pass

@view.route("/items/<id>", methods=["DELETE"])
def items_delete():
    pass

@view.route("/items", methods=["GET"])
def items_list():
    pass

@view.route("/send", methods=["POST"])
def items_link():
    pass

@view.route("/get", methods=["GET"])
def items_link():
    pass
