from application.app import db

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(20))
    password = db.Column(db.String(20))

    def __init__(self, login, password) -> None:
        self.login = login
        self.password = password

    def __str__(self) -> str:
        return f"{self.id}"

class Objects(db.Model):
    __tablename__ = "objects"
    id = db.Column(db.Integer, primary_key=True)
    id_us = db.Column(db.Integer)
    obj_prm_1 = db.Column(db.String(150))
    obj_prm_2 = db.Column(db.String(150))

    def __init__(self, id_us, obj_prm_1, obj_prm_2) -> None:
        self.id_us = id_us
        self.obj_prm_1 = obj_prm_1
        self.obj_prm_2 = obj_prm_2

    def __str__(self) -> str:
        return f"{self.id}"